terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.65"
    }
  }
  required_version = ">= 0.14.9"
}

provider "aws" {
  profile = "default"
  region  = "us-east-1"
}




# AWS resources for the Frontend
# resource "random_pet" "frontend" {
#   prefix = "review-docsite-${terraform.workspace}"
#   length = 4
# }
resource "aws_s3_bucket" "frontend" {
  bucket = "review-docsite"
  # access limits: "public-read" is for a public website with it's own endpoint. usful for deving, but maybe lock down later:
  acl           = "public-read"
  force_destroy = true
  website {
    index_document = "index.html"
    error_document = "404.html"
  }
  tags = {
    product = "test-review-docsite"
    owner   = "omiles"
    team    = "softserve"
  }
}

variable "upload_directory" {
  default = "../src/.vuepress/dist/"
}

# this is for the MIME type, so S3 knows what to map the type to:
# https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_object
variable "mime_types" {
  default = {
    htm      = "text/html"
    html     = "text/html"
    css      = "text/css"
    ttf      = "font/ttf"
    js       = "application/javascript"
    map      = "application/javascript"
    json     = "application/json"
    svg      = "image/svg+xml"
    ico      = "image/vnd.microsoft.icon"
    png      = "image/png"
    nojekyll = "text/html"
  }
}

resource "aws_s3_bucket_object" "frontend_dist" {
  for_each     = fileset(var.upload_directory, "**/*.*")
  bucket       = aws_s3_bucket.frontend.bucket
  key          = replace(each.value, var.upload_directory, "")
  source       = "${var.upload_directory}${each.value}"
  acl          = "public-read"
  etag         = filemd5("${var.upload_directory}${each.value}")
  content_type = lookup(var.mime_types, split(".", each.value)[length(split(".", each.value)) - 1])
}
