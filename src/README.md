# REview Docs: v0

## Introduction

Hi Matt and Mairon This is the documentation site for the Review Product.

Not sure you' have links, but you could. for example: [What is an LME?](https://resurety.com/2021/05/12/locational-marginal-emissions-a-force-multiplier-for-the-carbon-impact-of-clean-energy-programs/)

Really we can add anything. I imagin you'd want .gifs of screenshots showing how to access a thing or whatnot

### and more stuff in this subheading!

- and then you can do bullets like this
- which is a nice thing
- we can also put in emojis to show how much we care :cupid: :tada: :100:

I imagine you'll have lots of screenshots explaining what stuff is, it's pretty easy to do that:
![The Time Series Data Explorer view](./img/TimesereisDataExplorer.png)

::: tip Tip
Maybe you could have a section for each page, explaining some of the data sources and why it's important
:::

## Home 

Owen's naming suggestion: call the reveiw "Home" screen something descriptive. ("Portfolio Performance" or similar) as we don't really have a "home" it's just a common place to start

- suggest including an explanation of the bottom right plot. I'm not really sure what it's for. 

| Data Input | Source     | Note        |
| ---------- | ---------- | ----------- |
| Price data | Yes Energy | (if needed) |
| ...etc     | ...        | ...         |


Or if you want you can use code snippets...
```
...to say things with emphasis
```

Or to have actual code:
```py
print("hi Matt and Marion!")
```

## Monthly Overview

(suggestion: maybe call this one "Monthly Summary"? I personally steer away from "Overview")

- suggest including an explanation of the bottom right plot. I'm not really sure what it's for. 

## Time series explorer

(another naming suggestion: "monthly time series")

::: warning Note
Here's a note you can put in that's yellow and a warning
:::


::: tip Tip
Or a tip, these are green and therefor more friendly
:::

## etc...

